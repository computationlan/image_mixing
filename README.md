# Image mixing

This is an implementation of an algorithm for composing two images into one, such that the transition between the images appears smooth and natural. More information about the mathematics behind this algorithm can be found here:

https://www.cs.toronto.edu/~mangas/teaching/320/assignments/a3/spline83.pdf

**Examples**

The default mask used by the program is designed to vertically combine two images in the middle. To illustrate this setting, the program can be run with an image of an apple and an image of an orange. The result looks as following:

![apple.jpg](https://bitbucket.org/computationlan/image_mixing/raw/e62f12c1801defd3e247f012eafcc19ea42c4287/images/apple_thumbnail.jpg)     | ![orange.jpg](https://bitbucket.org/computationlan/image_mixing/raw/e62f12c1801defd3e247f012eafcc19ea42c4287/images/orange_thumbnail.jpg)   | ![apple_orange.jpg](https://bitbucket.org/computationlan/image_mixing/raw/e62f12c1801defd3e247f012eafcc19ea42c4287/images/result1_thumbnail.jpg) 
:---------:|:-----:|:-----
Image 1  |  Image 2 | Result

A variety of masks of any graphical complexity (binary image) can be used. An elliptical mask supplied in the images folder used to combine a picture of plumeria flower with an image of a maple leaf results in the following image:

![plumeria.jpg](https://bitbucket.org/computationlan/image_mixing/raw/e62f12c1801defd3e247f012eafcc19ea42c4287/images/plumeria_thumbnail.jpg)     | ![maple.jpg](https://bitbucket.org/computationlan/image_mixing/raw/e62f12c1801defd3e247f012eafcc19ea42c4287/images/maple_thumbnail.jpg)   | ![ellipse_mask.jpg](https://bitbucket.org/computationlan/image_mixing/raw/e62f12c1801defd3e247f012eafcc19ea42c4287/images/mask_thumbnail.jpg) | ![plumeria_maple.jpg](https://bitbucket.org/computationlan/image_mixing/raw/e62f12c1801defd3e247f012eafcc19ea42c4287/images/result2_thumbnail.jpg)
:---------:| :-----: |:-----:|:-----:
Image 1  |  Image 2 | Mask | Result

**Implementation**

1. Both images are transformed into Laplacian pyramids (stored as lists of images) encoding different frequency components of the original images. The first level contains the highest spatial frequencies, the next level has the highest remaining frequencies, etc. To build the pyramid, an image is blurred (Gaussian blur) and subtracted from the original, the resulting image of the highest frequencies is stored in an array. The blurred image is re-sized (to half of its original length and width), and the steps are repeated.

1. The mask is transformed into a Gaussian pyramid. For this, the initial mask image is blurred and stored in an array, then re-sized and the steps are repeated.

1. The two Laplacian pyramids are combined layer by layer by using the mask pyramid as linear weights.

1. Finally the blended pyramid layers are upsampled and summed together to produce the combined image.

**Instructions**

    # install the prerequisites 
    pip install pillow 
    # combine two images vertically 
    src/im_blending.py images/apple.jpg images/orange.jpg 
    # combine two images using an arbitrary mask src/im_blending.py
    images/plumeria.jpg images/maple.jpg --mask images/ellipse_mask.jpg --output out.jpg