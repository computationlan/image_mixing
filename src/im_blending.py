#!/usr/bin/env python3

from PIL import Image, ImageDraw
from im_lib import blend_images
import argparse

if __name__ == "__main__":
	
	parser = argparse.ArgumentParser()
	parser.add_argument("image1", help="first image file name")
	parser.add_argument("image2", help="second image file name")
	parser.add_argument("--mask", help="blend mask file name")
	parser.add_argument("--output", help="output file name")
	args = parser.parse_args()

	image1 = Image.open(args.image1)
	image2 = Image.open(args.image2)

	if args.mask:
		mask = Image.open(args.mask).convert("L")
	else:
		# Default mask is a vertical 50-50 split.
		mask = Image.new('L', image1.size, color=0)
		draw = ImageDraw.Draw(mask)
		draw.rectangle([0, 0, image1.size[0]//2, image1.size[1]], fill=255)
	
	result = blend_images(image1, image2, mask)

	if args.output:
		result.save(args.output)
	else:
		result.show()