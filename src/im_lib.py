from PIL import Image, ImageFilter, ImageChops, ImageDraw
import math

def num_levels(image):
	# How many levels it takes to get to a 1x1 image.
	return math.floor(math.log2(min(image.size)))

def downsample(image):
	image = image.resize((image.size[0]//2, image.size[1]//2))
	image = image.filter(ImageFilter.GaussianBlur(2))
	return image

def image_to_pyramid(image, levels):
	"""Build a Laplacian pyramid from the image."""
	pyramid = []
	for i in range(levels):
		downsampled = downsample(image)
		upsampled = downsampled.resize(image.size)
		# Subtract low-frequency image from the original to 
		# isolate the high frequencies.
		# Scale by 0.5x to stay within the 0..255 range.
		pyramid.append(ImageChops.subtract(image, upsampled, 2, 128))
		image = downsampled
	pyramid.append(image)
	return pyramid
	
def image_to_gauss_pyramid(image, levels):
	"""Build a Gaussian pyramid from the image."""
	pyramid = [image]
	for i in range(levels):
		pyramid.append(downsample(pyramid[-1]))
	return pyramid

def pyramid_to_image(pyramid):
	"""Restore a full image from the pyramid."""
	image = pyramid[-1]
	for layer in pyramid[-2::-1]:
		image = image.resize(layer.size)
		image = image.filter(ImageFilter.GaussianBlur(2))
		# Apply scaling again to revert the transform done by 
		# image_to_pyramid and stay within 0..255 range.
		image = image.point(lambda x: x/2)
		image = ImageChops.add(layer, image, 0.5, -256)
	return image

def show_pyramid(pyramid):
	for layer in pyramid:
		layer.show()

def blend_images(image1, image2, mask):
	levels = num_levels(image1)
	pyramid1 = image_to_pyramid(image1, levels)
	pyramid2 = image_to_pyramid(image2.resize(image1.size, Image.LANCZOS), levels)
	mask_pyramid = image_to_gauss_pyramid(mask.resize(image1.size, Image.LANCZOS), levels)
	# Combine each level independently.
	blended_pyramid = [Image.composite(pyramid1[i], pyramid2[i], mask_pyramid[i]) for i in range(len(pyramid1))]
	return pyramid_to_image(blended_pyramid)
